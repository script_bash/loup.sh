#!/bin/bash

# Jeux de gestion de loup et de son poids
# Dépendences = bc


if command -v "$1" &>/dev/null;then
	echo "bc n'est pas installé !"
	exit 55
else
	clear
fi

poids_debut_kilo=45
calories_min_jour=2000
capacite_estomac_kilo=10
estomac=0
calories_1k_viande=3000
gagner_perdre_1k=8000
reserve=0
chasse_jour=0
poids_alpha="$poids_debut_kilo"

main () {

	clear
	echo "$estomac"
	echo "$reserve"
	echo "$poids_alpha"
	echo "Que voulez-vous faire ?"
	echo ""
	echo "1. Manger"
	echo "2. Chasser"
	echo "3. Dormir"
	echo "4. Infos"
	echo "5. Quitter"

	read choix

	case "$choix" in

		1)
			manger
			;;
		2)
			chasser
			;;
		3)
			dormir
			;;
		4)
			infos
			;;
		5)
			exit 1
			;;
		*)
			echo "Mauvais choix"
			;;
	esac
	return 0
}

manger () {
	if [ "$reserve" -gt 0 ];then
		if [ "$estomac" -lt "$capacite_estomac_kilo" ];then
			estomac=$(echo "scale=2;""$estomac""+1" | bc)
			reserve=$(echo "scale=2;""$reserve""-1" | bc)
		else
			echo "Votre estomac est rempli, appuyez sur une entrer pour continuer."
			read
		fi
	else
		echo "Votre réserve est vide, chassez pour la remplir"
		read
	fi
	return 0
}

chasser () {
	if [ "$chasse_jour" == 0 ];then
		if [ $((RANDOM % 10)) == 3 ];then
			reserve=$(echo "scale=2;""$reserve""+55" | bc)
			echo "La chasse a été fructueuse, appuyez sur entrer touche pour continyer."
			chasse_jour=1
			read
		else
			echo "La chasse n'a pas été fructueuse, appuyez sur une entrer pour continuer."
			chasse_jour=1
			read
		fi
	else
		echo "Vous avez déjà chassé aujourd'hui. Appuyez sur entrer pour continuer"
		read
	fi
	return 0
}

dormir () {
	chasse_jour=0
	digestion
	estomac=0
	return 0
}

digestion () {
	poids_alpha=$(echo "scale=2;((""$estomac""*3000)-""$calories_min_jour"")/""$gagner_perdre_1k""+""$poids_alpha""" | bc)
	return 0
}

infos () {
	local p_a=$(echo "scale=0;""$poids_alpha""/1" | bc)
	if [ "$p_a" -lt 40 ];then
		echo "Attention, vous faites ""$poids_alpha""kilos, vous êtes en sous-nuttrition !"
		read
	elif [ "$p_a" -lt 50 ];then
		echo "Vous faites ""$poids_alpha""kilos, ce qui est un poids santé pour un loup mâle de votre taille."
		read
	elif [ "$p_a" -lt 60 ];then
		echo "Vous êtes à ""$poids_alpha""kilos, ce qui fait de vous un gros sac"
		read
	fi
	return 0
}

while true
do
	main
done
